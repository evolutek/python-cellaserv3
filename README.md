# python-cellaserv3

Python3 client library for cellaserv3.

## Features

* Client
* Service
* Services stub

See example usage in `examples/`.

## Install

Installing `python-cellaserv3`:

    $ git clone ssh://git@bitbucket.org/evolutek/python-cellaserv3.git
    $ cd python-cellaserv3
    $ git submodule init
    $ git submodule update cellaserv/protobuf
    $ python setup.py develop

## Testing

Run:

    $ pytest

## Authors

- Rémi Audebert, Evolutek 2012-2018
- Benoît Reitz, Evolutek 2013-2015
- Adrien Schildknecht, Evolutek 2013-2015
- Vincent Gatine, Evolutek 2014-2015
- Corentin Vigourt, Evolutek 2014-2020
